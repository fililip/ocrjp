const express = require("express");
const bodyParser = require("body-parser");
const http = require("http");
const fs = require("fs");
const tesseract = require("tesseract.js");
const jishoApi = require("unofficial-jisho-api");
const jisho = new jishoApi();

const app = express();
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ limit: "50mb" }));
app.use("/res", express.static("res", { root: __dirname }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.sendFile("index.html", { root: __dirname });
});

const worker = tesseract.createWorker({ langPath: "lang" });
(async () => {
    await worker.load();
    await worker.loadLanguage("jpn");
    await worker.initialize("jpn");
})();

app.post("/", (req, res) => {
    const base64string = req.body.base64.split(",")[1];
    const buffer = Buffer.from(base64string, "base64");
    if (buffer.length > 50000) { res.send({ success: false }); return; }
    (async () => {
        let { data: { text } } = await worker.recognize(buffer);
        text = text.trim().replace(/\s/g, "");
        /* jisho.searchForPhrase(text).then(result => {
            res.send({ text: text, result: result });
        }); */
        res.send({ text: text, success: true });
    })();
});

app.listen(8080);